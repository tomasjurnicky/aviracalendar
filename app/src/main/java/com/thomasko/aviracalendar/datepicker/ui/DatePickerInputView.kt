package com.thomasko.aviracalendar.datepicker.ui

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.thomasko.aviracalendar.R
import kotlinx.android.synthetic.main.base__date_input_layout.view.*

class DatePickerInputView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    init {
        initialize(context)
        readAttributes(context, attrs)
    }

    private fun initialize(context: Context?) {
        inflate(context, R.layout.base__date_input_layout, this)
        orientation = LinearLayout.VERTICAL
        maturityDateEditText.inputType = InputType.TYPE_NULL
    }

    private fun readAttributes(context: Context, attrs: AttributeSet?) {
        if (attrs != null) {
            val typedArray = context.obtainStyledAttributes(attrs, R.styleable.DatePickInputView)
            try {
                val hint = typedArray.getString(R.styleable.DatePickInputView_hint)
                inputLayout.hint = hint
            } finally {
                typedArray.recycle()
            }
        }
    }

    override fun isClickable() = true

    fun getText() = maturityDateEditText.text

    fun setText(text: String) = maturityDateEditText.setText(text)

    fun setHint(hint: String) {
        inputLayout.hint = hint
    }

    override fun setOnClickListener(l: View.OnClickListener) {
        maturityDateEditText.setOnClickListener(l)
        maturityDatePickButton.setOnClickListener(l)
    }


}