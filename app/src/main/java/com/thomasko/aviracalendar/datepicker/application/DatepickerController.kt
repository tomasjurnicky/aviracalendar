package com.thomasko.aviracalendar.datepicker.application

import android.app.DatePickerDialog
import com.thomasko.aviracalendar.base.application.AndroidController
import java.util.*
import javax.inject.Inject


class DatepickerController @Inject constructor() {

    @Inject lateinit var androidController: AndroidController

    internal fun showDatepickerWithActualDate(listener: DatePickerDialog.OnDateSetListener) {
        val currentTime = Calendar.getInstance()
        DatePickerDialog(
                androidController.baseActivity,
                listener,
                currentTime.get(Calendar.YEAR),
                currentTime.get(Calendar.MONTH),
                currentTime.get(Calendar.DAY_OF_MONTH)
        ).show()
    }
}
