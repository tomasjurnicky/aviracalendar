package com.thomasko.aviracalendar.addbirthday.dataaccess

import com.thomasko.aviracalendar.addbirthday.application.BirthdayService
import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail
import io.reactivex.Single
import io.realm.Realm
import io.realm.Sort
import org.joda.time.LocalDate

internal class BirthdayServiceImpl : BirthdayService {

    override fun addBirthdayData(item: BirthdayDetail) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val birthday = realm.createObject(BirthdayDetail::class.java)
        birthday.date = item.date
        birthday.name = item.name
        realm.commitTransaction()
    }

    override fun getBirthdayData(): Single<List<BirthdayDetail>> = Single.create<List<BirthdayDetail>> {
        val realm = Realm.getDefaultInstance()
        val data = realm.where(BirthdayDetail::class.java).findAll().sort("date", Sort.ASCENDING).filter { it.date!! >= LocalDate.now().toDate().time }
        it.onSuccess(data)
    }
}