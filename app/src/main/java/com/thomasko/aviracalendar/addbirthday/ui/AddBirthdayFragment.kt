package com.thomasko.aviracalendar.addbirthday.ui

import android.os.Bundle
import android.view.View
import com.thomasko.aviracalendar.R
import com.thomasko.aviracalendar.base.injection.injector
import com.thomasko.aviracalendar.base.ui.lifecycle.MvpFragment
import com.thomasko.aviracalendar.utils.afterTextChanged
import com.thomasko.aviracalendar.utils.calendarIntentAddEvent
import com.thomasko.aviracalendar.utils.showToast
import kotlinx.android.synthetic.main.addbirthday__fragment.*
import org.joda.time.LocalDate

internal class AddBirthdayFragment : MvpFragment<AddBirthdayPresenter, AddBirthdayView>(), AddBirthdayView {

    companion object {
        fun create(): AddBirthdayFragment = AddBirthdayFragment()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addToCalendar.setOnClickListener { presenter.couldAddToCalendar() }
        name.afterTextChanged { presenter.titleEvent = it }
        datePicker.setOnClickListener { presenter.showDatePicker() }
        microphone.setOnClickListener { presenter.speechRecognize() }
    }

    override fun managerTag() = "AddBirthdayFragment"

    override fun getMvpView() = this

    override fun inject() = activity.injector.inject(this)

    override fun getLayoutResourceId() = R.layout.addbirthday__fragment

    override fun getTitle() = context.getString(R.string.add_birthday)

    override fun showValidationError() =
            context.showToast(getString(R.string.add_birthday_validate_error))

    override fun openCalendar(beginTime: LocalDate, titleEvent: String) =
            context.calendarIntentAddEvent(beginTime, titleEvent)

    override fun setDate(drawingTime: String) = datePicker.setText(drawingTime)

    override fun setName(text: String) = name.setText(text)
}