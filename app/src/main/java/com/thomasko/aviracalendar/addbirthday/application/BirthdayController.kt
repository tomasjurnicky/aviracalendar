package com.thomasko.aviracalendar.addbirthday.application

import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BirthdayController @Inject constructor() {

    @Inject lateinit var birthdayService: BirthdayService

    internal fun getBirthdays() = birthdayService.getBirthdayData()

    internal fun addBirthday(data: BirthdayDetail) = birthdayService.addBirthdayData(data)
}