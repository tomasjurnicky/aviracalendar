package com.thomasko.aviracalendar.addbirthday.ui

import com.thomasko.aviracalendar.base.ui.lifecycle.MvpView
import org.joda.time.LocalDate

interface AddBirthdayView : MvpView {

    fun showValidationError()

    fun openCalendar(beginTime: LocalDate, titleEvent: String)

    fun setDate(drawingTime: String)

    fun setName(text: String)
}