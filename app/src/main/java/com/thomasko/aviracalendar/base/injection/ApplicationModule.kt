package com.thomasko.aviracalendar.base.injection

import android.app.Application
import com.thomasko.aviracalendar.AviraCalendarApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class ApplicationModule(val application: AviraCalendarApplication) {

    @Singleton
    @Provides
    fun provideApplication(): Application = application

}