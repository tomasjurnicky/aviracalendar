package com.thomasko.aviracalendar.base.ui.list

import android.support.v7.widget.RecyclerView

abstract class BaseListAdapter<D, BH : BaseHolder>(
        internal var list: List<D>,
        internal var itemListener: OnListItemClickListener<D, BH>? = null
) : RecyclerView.Adapter<BH>() {

    interface OnListItemClickListener<D, BH> {
        fun onListItemClicked(position: Int, item: D, holder: BH)
    }

    override fun onBindViewHolder(holder: BH, position: Int) {
        holder.itemView.setOnClickListener { itemListener?.onListItemClicked(position, list[position], holder) }
        bindData(holder, list[position], position)
    }

    override fun getItemCount() = list.size

    protected abstract fun bindData(holder: BH, data: D, position: Int)
}