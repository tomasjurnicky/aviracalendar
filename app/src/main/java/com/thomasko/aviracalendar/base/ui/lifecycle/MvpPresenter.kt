package com.thomasko.aviracalendar.base.ui.lifecycle

import com.thomasko.aviracalendar.base.infrastructure.Logger
import com.thomasko.aviracalendar.base.infrastructure.i

abstract class MvpPresenter<V : MvpView> : Logger {

    protected var view: V? = null

    init {
        i { "===== Constructor" }
    }

    internal fun bindView(view: V) {
        i { "===== BindView" }
        this.view = view

        updateAllFieldsOnView()
    }

    internal open fun onAfterStart() =
            i { "===== onAfterStart" }

    internal open fun onAfterCreate() =
            i { "===== onAfterCreate" }

    internal open fun updateAllFieldsOnView() =
            i { "===== updateAllFieldsOnView" }

    internal fun unbindView() {
        notifyBeforeViewUnBind()
        i { "===== UnbindView" }
        view = null
    }

    internal open fun notifyBeforeViewUnBind() {}

    internal fun destroyView() =
            i { "===== DestroyView" }
}
