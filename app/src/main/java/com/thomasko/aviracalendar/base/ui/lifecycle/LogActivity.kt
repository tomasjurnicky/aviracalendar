package com.thomasko.aviracalendar.base.ui.lifecycle

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import com.thomasko.aviracalendar.base.infrastructure.Logger
import com.thomasko.aviracalendar.base.infrastructure.i

abstract class LogActivity : AppCompatActivity(), Logger {

    /**
     * Provide the ID of the component which is going to contain the main content for this activity
     *
     * @return the resource ID of the main content container.
     */
    @get:IdRes
    abstract val mainContentResourceId: Int

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        i { "==A== onCreate" }
    }

    override fun onResume() {
        super.onResume()
        i { "==A== onResume" }
    }

    override fun onDestroy() {
        super.onDestroy()
        i { "==A== onDestroy" }
    }

    override fun onPause() {
        super.onPause()
        i { "==A== onPause" }
    }
}