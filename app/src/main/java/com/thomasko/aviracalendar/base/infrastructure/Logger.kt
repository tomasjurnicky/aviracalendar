package com.thomasko.aviracalendar.base.infrastructure

import android.util.Log
import com.thomasko.aviracalendar.BuildConfig

/**
 * Convenient logger class.
 *
 * By default, logging is enabled when current build is the debug one.
 */
internal interface Logger {
    private companion object {
        private const val MAX_TAG_LENGTH = 23
    }

    val tagName: String
        get() = with(javaClass.simpleName) {
            if (length <= MAX_TAG_LENGTH) this else substring(0, MAX_TAG_LENGTH)
        }

    val enabled: Boolean
        get() = BuildConfig.DEBUG
}

internal inline fun Logger.i(thr: Throwable? = null, msg: () -> Any?) {
    log(this, msg, thr,
            { tag, m -> Log.i(tag, m) },
            { tag, m, t -> Log.i(tag, m, t) })
}

private inline fun log(logger: Logger, msg: () -> Any?, thr: Throwable?,
                       f: (String, String) -> Unit,
                       fThr: (String, String, Throwable) -> Unit) {
    if (logger.enabled) {
        val tag = logger.tagName
        with(msg()?.toString() ?: "null") {
            if (thr != null) fThr(tag, this, thr)
            else f(tag, this)
        }
    }
}