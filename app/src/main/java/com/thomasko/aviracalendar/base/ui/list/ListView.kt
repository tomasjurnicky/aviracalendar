package com.thomasko.aviracalendar.base.ui.list

import com.thomasko.aviracalendar.base.ui.lifecycle.MvpView

interface ListView<E> : MvpView {

    fun setItems(items: List<E>)

    fun notifyDataSetChanged()
}
