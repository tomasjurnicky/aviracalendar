package com.thomasko.aviracalendar.base.injection

import android.content.Context

internal interface ApplicationComponentProvider {
    val applicationComponent: ApplicationComponent
}

internal val Context.injector: ApplicationComponent
    get() = (applicationContext as ApplicationComponentProvider).applicationComponent
