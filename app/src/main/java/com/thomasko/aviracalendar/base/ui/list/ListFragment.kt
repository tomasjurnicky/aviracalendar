package com.thomasko.aviracalendar.base.ui.list

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.thomasko.aviracalendar.R
import com.thomasko.aviracalendar.base.ui.lifecycle.MvpFragment
import kotlinx.android.synthetic.main.base__list_fragment.*


abstract class ListFragment<
        P : ListPresenter<V, E>,
        V : ListView<E>, E> : MvpFragment<P, V>(), ListView<E> {

    override fun getLayoutResourceId() = R.layout.base__list_fragment

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = createLayoutManager()
        recyclerView.layoutManager = layoutManager

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val lastPostion = layoutManager.findLastVisibleItemPosition()
                if (lastPostion == (recyclerView?.adapter?.itemCount ?: 0) - 1)
                    if (!presenter.isLoading) {
                        presenter.onLastPositionScrolled()
                        presenter.isLoading = true
                    }
            }
        })

        if (hasDividers()) {
            recyclerView.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        }
    }

    abstract fun hasDividers(): Boolean

    /**
     * Override this method when you want use different manager for recyclerview
     *
     * @return RecyclerView.LayoutManager
     */
    open fun createLayoutManager() = LinearLayoutManager(activity)

    override fun setItems(items: List<E>) {
        if (recyclerView.adapter == null) {
            recyclerView.adapter = createAdapter(items)
        } else {
            val adapter = recyclerView.adapter as BaseListAdapter<E, out BaseHolder>            //TODO: find better smart cast solution
            adapter.list = items
        }
    }

    protected abstract fun createAdapter(items: List<E>): BaseListAdapter<E, out BaseHolder>

    override fun notifyDataSetChanged() {
        recyclerView.adapter?.notifyDataSetChanged()
    }
}
