package com.thomasko.aviracalendar.base.application

import android.content.Intent
import com.thomasko.aviracalendar.base.ui.lifecycle.BaseActivity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AndroidController @Inject constructor() {

    internal lateinit var baseActivity: BaseActivity

    internal var activityResultListener: ActivityResultListener? = null

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        this.activityResultListener?.onActivityResult(requestCode, resultCode, data)
    }
}