package com.thomasko.aviracalendar.base.ui.lifecycle

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thomasko.aviracalendar.base.infrastructure.Logger
import com.thomasko.aviracalendar.base.infrastructure.i

abstract class LogFragment : Fragment(), Logger, Tagger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        i { "===== onCreate" }

        inject()
    }

    abstract fun inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        i { "===== onCreateView" }
        return null
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        i { "===== onViewCreated" }
    }

    override fun onResume() {
        super.onResume()
        i { "===== onResume" }
    }

    override fun onPause() {
        super.onPause()
        i { "===== onPause" }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        i { "===== onDestroyView" }
    }

    override fun onDestroy() {
        super.onDestroy()
        i { "===== onDestroy" }
    }
}
