package com.thomasko.aviracalendar.base.ui.lifecycle

internal interface Tagger {
    fun managerTag(): String
}
