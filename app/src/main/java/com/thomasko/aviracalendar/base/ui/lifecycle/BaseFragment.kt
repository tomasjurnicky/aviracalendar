package com.thomasko.aviracalendar.base.ui.lifecycle

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thomasko.aviracalendar.R

abstract class BaseFragment : LogFragment() {

    @SuppressLint("ResourceType")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutResourceId(), container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getTitle() ?: context.getString(R.string.app_name))
    }

    @IdRes
    abstract fun getLayoutResourceId(): Int

    private fun setTitle(title: String) =
            (activity as AppCompatActivity).supportActionBar?.apply {
                setTitle(title)
                subtitle = null
            }

    protected abstract fun getTitle(): String?

    protected fun getBaseActivity(): BaseActivity? = activity?.let {
        if (activity is BaseActivity) activity as BaseActivity
        else throw RuntimeException("Parent activity is not an instance of BaseActivity")
    }

}
