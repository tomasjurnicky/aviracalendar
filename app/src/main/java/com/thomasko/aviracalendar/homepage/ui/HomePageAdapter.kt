package com.thomasko.aviracalendar.homepage.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.thomasko.aviracalendar.R
import com.thomasko.aviracalendar.base.ui.list.BaseHolder
import com.thomasko.aviracalendar.base.ui.list.BaseListAdapter
import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail
import kotlinx.android.synthetic.main.homepage__list_item.view.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class HomePageAdapter(
        val data: List<BirthdayDetail>
) : BaseListAdapter<BirthdayDetail, HomePageAdapter.HomePageBH>(data) {

    class HomePageBH(itemView: View) : BaseHolder(itemView)

    override fun bindData(holder: HomePageBH, data: BirthdayDetail, position: Int) {
        holder.itemView.apply {
            itemName.text = data.name
            itemDate.text = DateTime(data.date).toString(DateTimeFormat.longDate())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomePageBH = HomePageBH(LayoutInflater.from(parent.context)
            .inflate(R.layout.homepage__list_item, parent, false))
}