package com.thomasko.aviracalendar.homepage.ui

import android.os.Bundle
import android.view.View
import com.thomasko.aviracalendar.R
import com.thomasko.aviracalendar.addbirthday.ui.AddBirthdayFragment
import com.thomasko.aviracalendar.base.injection.injector
import com.thomasko.aviracalendar.base.ui.list.BaseHolder
import com.thomasko.aviracalendar.base.ui.list.BaseListAdapter
import com.thomasko.aviracalendar.base.ui.list.ListFragment
import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail
import com.thomasko.aviracalendar.utils.replaceFragment
import kotlinx.android.synthetic.main.homepage__fragment.*

internal class HomePageFragment : ListFragment<HomePagePresenter, HomePageView, BirthdayDetail>(), HomePageView {

    companion object {
        fun create(): HomePageFragment = HomePageFragment()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addBirthday.setOnClickListener {
            getBaseActivity()?.replaceFragment(AddBirthdayFragment.create(), true)
        }
        presenter.readBirthday()
    }

    override fun getLayoutResourceId() = R.layout.homepage__fragment

    override fun createAdapter(items: List<BirthdayDetail>): BaseListAdapter<BirthdayDetail, out BaseHolder> =
            HomePageAdapter(items)

    override fun managerTag() = "HomePageFragment"

    override fun getMvpView() = this

    override fun inject() = activity.injector.inject(this)

    override fun getTitle() = context.getString(R.string.homepage)

    override fun hasDividers() = true
}
