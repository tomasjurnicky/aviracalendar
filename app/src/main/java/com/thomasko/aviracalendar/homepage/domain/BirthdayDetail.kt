package com.thomasko.aviracalendar.homepage.domain

import io.realm.RealmObject


open class BirthdayDetail : RealmObject() {
    internal var name: String? = null
    internal var date: Long? = null
}