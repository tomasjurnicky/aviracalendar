package com.thomasko.aviracalendar.homepage.injection

import com.thomasko.aviracalendar.addbirthday.application.BirthdayService
import com.thomasko.aviracalendar.addbirthday.dataaccess.BirthdayServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class DataModule {

    @Provides
    @Singleton
    fun provideBirthdayService(): BirthdayService = BirthdayServiceImpl()

}
