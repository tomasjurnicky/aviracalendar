package com.thomasko.aviracalendar

import android.app.Application
import com.thomasko.aviracalendar.base.injection.ApplicationComponent
import com.thomasko.aviracalendar.base.injection.ApplicationComponentProvider
import com.thomasko.aviracalendar.base.injection.ApplicationModule
import com.thomasko.aviracalendar.base.injection.DaggerApplicationComponent
import io.realm.Realm
import io.realm.RealmConfiguration
import net.danlew.android.joda.JodaTimeAndroid

internal class AviraCalendarApplication : Application(), ApplicationComponentProvider {

    override lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
        initRealm()
        initInjection()
    }

    private fun initInjection() {
        applicationComponent = createApplicationComponent()
        applicationComponent.inject(this@AviraCalendarApplication)
    }

    private fun initRealm() {
        Realm.init(this)
        val config = RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build()
        Realm.setDefaultConfiguration(config)
    }

    private fun createApplicationComponent() = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()!!
}